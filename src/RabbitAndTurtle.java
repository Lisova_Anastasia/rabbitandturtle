/**
 * Класс для создания и запуска потоков:
 * Черепахи с приоритетом 1 и Кролика
 * с приоритетом 10
 *
 * @author Лисова Анастасия, 17ИТ17
 */
public class RabbitAndTurtle {
    public static void main(String[] args) {
        System.out.println("Старт!");
        new AnimalThread("Черепаха", 1);
        new AnimalThread("Кролик", 10);
    }
}
