/**
 * Класс конструктор потоков
 *
 * @author Лисова Анастасия, 17ИТ17
 */
public class AnimalThread extends Thread{
    private static final int STEP = 10;
    private static final int FRONTIER = 500;
    private Thread thread;

    /**
     * Конструктор потока
     *
     * @param name имя потока
     * @param priority приоритет потока
     */
    AnimalThread(String name, int priority){
        thread = new Thread(this, name);
        thread.setPriority(priority);
        thread.start();
    }

    /**
     * Метод, выполняющий функцию счётчика метров
     */
    public void run(){
        for (int i = 0; i < FRONTIER; i += STEP) {
            System.out.println(thread.getName() + " - " + i + " метров");
        }
        if(getPriority() == 1){
            setPriority(10);
        }
        if(getPriority() == 10){
            setPriority(1);
        }
        for (int i = FRONTIER; i < 1001; i += STEP) {
            System.out.println(thread.getName() + " - " + i + " метров");
        }
    }
}
